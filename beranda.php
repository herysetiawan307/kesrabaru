<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/components/templates/main.php";
    require_once dirname(__FILE__)."/class/native_connect.php";

    
    //Call Template
    $template = new Template();

    //Start HTML
    $template->pageTitle="KESRA ADM";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->startContent();

    //print_r($_SESSION['lvl']);
    
?>

<!-- Start Box -->
<div class="row">
    <!-- Box New Booking -->
    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_new_booking.php"><span class="info-box-icon bg-aqua"><i class="fa fa-calendar-plus-o"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>New</strong></span>
                <span class="info-box-more">Membuat Booking Baru</span>
            </div>
        </div>
    </div> -->
    <!-- Box Edit -->
    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/data_edit_booking.php"><span class="info-box-icon bg-olive"><i class="fa fa-edit"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Edit</strong></span>
                <span class="info-box-more">Edit Booking</span>
            </div>
        </div>
    </div> -->
    <!-- Box Pembayaran -->
    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_pembayaran.php"><span class="info-box-icon bg-navy"><i class="fa fa-download"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Pembayaran</strong></span>
                <span class="info-box-more">Pembayaran Booking</span>
            </div>
        </div>
    </div> -->
    <!-- Box Cancel -->
    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/data_cancel_booking.php"><span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Batal</strong></span>
                <span class="info-box-more">Pembatalan Booking</span>
            </div>
        </div>
    </div>
</div> -->
<!-- End Box -->

<!-- Search with Booking Date, Event Date, and Event Name -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong> DASBORD </strong>      
        <?php $template->conBox();?>
        <div class="row">
            <div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-md-4">
                            <img src="<?= MAIN_URL ?>/images/bg-01.jpg" style="resize:none;width:700px;">
                        </div>
                    </div>
                    </div>
                    <div>&nbsp</div>
                    <div>&nbsp</div>
                    <div class="row">  
                    <div><center><H2><strong> LIST LPJ PENERIMA HIBAH </strong></H2></center></div>   
                    </div>
            <div class="row">            
                <!-- Tanggal Booking -->              
                <!-- <div class="col-md-3">
                
                    <div class="form-group">
                        <label>Tanggal Booking : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_booking">
                        </div>
                    </div>
                </div> -->
                <!-- Tanggal Event -->
                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_event">
                        </div>
                    </div>
                </div> -->
                <!-- Nama Event -->
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-building"></i>
                            </div>
                            <input class="form-control" type="text" id="nama_event">
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Table -->
            <div class="col-md-12">
                    <table class="table table-responsive table-striped table-hover">
                        <thead align="center" style="background-color:#00a65a; font-weight:bold;">
                            <tr style="color:white;">
                                <td>No.SK</td>
                                <td>Nama Lembaga</td>
                                <td>Alamat</td>
                                <td>Peruntukan</td>
                                <td>Nominal</td>

                            </tr>
                        </thead>
                        <?php
				
				
                $x=mysqli_query($connect,"SELECT * FROM lpj");
                
                while($a=mysqli_fetch_array($x)){?>
                    <tr>
                        
                        <td><?= $a['sk'] ?></td>
                        <td><?= $a['lembaga'] ?></td>
                        <td><?= $a['alamat'] ?></td>
                        <td><?= $a['peruntukan'] ?></td>
                        <td><?= $a['jml_hibah'] ?></td>       
                    </tr>
                    <?php }
                ?>
                        <tbody id="tbody">
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php $template->endContent(); ?>
<!-- Place Script Here -->

<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>