<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";

        //Define Connection -> Database
        //$db = new Database();
        //$db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="Input Pertanggungjawaban  | Admin Kesra";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Input Data Pertanggungjawaban";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Satuan -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/add_LPJ.php" enctype="multipart/form-data">
                <div style="margin-left:15px">
                    <h4><u>.</u></h4>
                </div>
                <!-- sk -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomor SK</label>
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="sk" placeholder="Nomor SK">
                    </div>
                </div>
                
                <!-- nama lembaga -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lembaga</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="lembaga" placeholder="Nama Lembaga">
                    </div>
                </div>
                <!-- Alamat lembaga -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat lembaga</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="alamat" placeholder="Alamat lembaga"></textarea>
                    </div>
                </div>
                <!-- peruntukan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Peruntukan</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="peruntukan" placeholder="Peruntukan"></textarea>
                    </div>
                </div>
                 <!-- Tanggal Pengumpulan -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Pengumpulan</label>
                        
                    <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="pengumpulan" placeholder="Tanggal Pengiriman">
                    </div>
                </div>
                <!-- nama ketua -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Ketua</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nama" placeholder="Nama Ketua">
                    </div> 
                </div>
                 <!-- Alamat lembaga -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Ketua</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="alamat_ketua" placeholder="Alamat Ketua"></textarea>
                    </div>
                </div>

                <!-- Phone -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="phone" placeholder="Phone">
                    </div>
                </div>
                 <!-- Nominal -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Nominal Hibah</label>

                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="nominal" placeholder="Nominal Hibah">
                            <span class="input-group-addon">.00</span>
                        </div>

                    </div>
                </div>
                 <!-- Rekening koran -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Rekening Koran(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="rek" id="rek">
                    </div>
                </div>
                 <!-- lpj -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Nilai LPJ(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="lpj" placeholder="Nilai LPJ">
                    </div>
                </div>     
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>