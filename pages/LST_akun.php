<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
 <?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="Admin Kesra | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> List Akun";
    $template->startContent();
?>

<!-- Log -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover" id="tsystem">
                        <thead>
                            <tr>
                                <td>Nama Lengkap</td>
                                <td>NIP</td>
                                <td>Alamat</td>
                                <td>No.Telp</td>
                                <td>Level</td>
                                <td>Username</td>
                                <td>Action</td>

                            </tr>
                        </thead>
                <?php
				
				
                $x=mysqli_query($connect,"SELECT * FROM akun");
                
                while($a=mysqli_fetch_array($x)){?>
                    <tr>
                        
                    <td><?= $a['nama_lengkap'] ?></td>
                        <td><?= $a['nip'] ?></td>
                        <td><?= $a['alamat'] ?></td>
                        <td><?= $a['telp'] ?></td>
                        <td><?= $a['lvl'] ?></td>
                        <td><?= $a['username'] ?></td>
                        <td>
                            <a href="#myModal" data-toggle="modal" href="javascript:void(0)" data-id="<?= $a["nip"]; ?>">
                            <button class="btn btn-sm bg-olive"><span class="glyphicon glyphicon-edit"></span> Edit</button></a>

                            <a class="delete-item" href="javascript:void(0)" data-id="<?= $a["nip"]; ?>">
                            <button title="Delete Akun" class="btn btn-sm btn-danger"> Delete</button></a>
                        </td>            
                    </tr>
                    <?php }
                ?>


                        <tbody>
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Modal -->
<?php
    $template->startModal();
    $template->conModal();
    $template->footModal();
?>



<!-- Place Script Here -->
<!--BELUM MAU DELETE DAN UPDATE-->
<script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var nip = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/delete_akun.php",
                data: "nip="+nip
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>

    <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    <!-- EDIT AKUN -->
    <script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            
            $.ajax({
                type : 'post',
                url : 'FRM_edit_akun.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);
                }
            });
         });
    });
  </script>
    
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>