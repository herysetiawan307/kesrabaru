<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";
        
    //Call Template
        $template = new Template();
        
        
                   
        $id = $_REQUEST["rowid"];
        $sql = mysqli_query($connect,"SELECT * FROM lpj WHERE sk='id'");
        $result = mysqli_fetch_array($sql);
?>
            

            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/update_lpj.php">
                <div style="margin-left:15px">
                    <h4><u>Edit Data Akun</u></h4>
                </div>
               <!-- sk -->
               <div class="form-group">
                    <label class="col-sm-2 control-label">Nomor SK</label>
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="sk" placeholder="Nomor SK" value="<?= $result["sk"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- nama lembaga -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lembaga</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="lembaga" placeholder="Nama Lembaga" value="<?= $result["lembaga"]; ?>">
                    </div>
                </div>
                <!-- Alamat lembaga -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat lembaga</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="alamat" placeholder="Alamat lembaga"><?= $result["alamat"]; ?></textarea>
                    </div>
                </div>
                <!-- peruntukan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Peruntukan</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="peruntukan" placeholder="Peruntukan"><?= $result["peruntukan"]; ?></textarea>
                    </div>
                </div>
                 <!-- Tanggal Pengumpulan -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Pengumpulan</label>
                        
                    <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="pengumpulan" placeholder="Tanggal Pengiriman"value="<?= $result["tgl_pengumpulan"]; ?>">
                    </div>
                </div>
                <!-- nama ketua -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Ketua</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nama" placeholder="Nama Ketua" value="<?= $result["ketua"]; ?>">
                    </div> 
                </div>
                 <!-- Alamat lembaga -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Ketua</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:300px;height:100px;" name="alamat_ketua" placeholder="Alamat Ketua"><?= $result["alamat_ketua"]; ?></textarea>
                    </div>
                </div>

                <!-- Phone -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="phone" placeholder="Phone" value="<?= $result["phone"]; ?>">
                    </div>
                </div>
                 <!-- Nominal -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Nominal Hibah</label>

                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="nominal" placeholder="Nominal Hibah" value="<?= $result["jml_hibah"]; ?>">
                            <span class="input-group-addon">.00</span>
                        </div>

                    </div>
                </div>
                 <!-- Rekening koran -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Rekening Koran(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="rek" id="rek" value="<?= $result["rek_koran"]; ?>">
                    </div>
                </div>
                 <!-- lpj -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Nilai LPJ(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="lpj" placeholder="Nilai LPJ" value="<?= $result["nilai_lpj"]; ?>">
                    </div>
                </div>     
                
              
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="simpan btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="reset btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>

<script>
    $(document).ready(function(){
        $(".select2").select2();
    });
</script>