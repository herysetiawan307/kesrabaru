<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
 <?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Connection -> Database
    //$db = new Database();
    //$db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="List LPJ | Admin Kesra";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> List Data LPJ";
    $template->startContent();
?>

<!-- Log -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover" id="tsystem">
                        <thead>
                            <tr>
                            <td>No SK</td>
                            <td>Nama Lembaga</td>
                            <td>Alamat Lembaga</td>
                            <td>Peruntukan</td>
                            <td>Tgl Pengumpulan</td>
                            <td>Nominal</td>
                            <td>Rek_koran & LPJ</td>
                            
                            </tr>
                        </thead>
                
                <?php
				//data 	BELOM MAU		
                $x=mysqli_query($connect,"SELECT * FROM lpj");
                
                while($a=mysqli_fetch_array($x)){?>
                    <tr>
                        
                    <td><?= $a['sk'] ?></td>
                        <td><?= $a['lembaga'] ?></td>
                        <td><?= $a['alamat'] ?></td>
                        <td><?= $a['peruntukan'] ?></td>
                        <td><?= $a['tgl_pengumpulan'] ?></td>
                        <td><?= $a['jml_hibah'] ?></td>
                        <td>
                            <a href="<?= MAIN_URL ?>/file/rekening/<?= $a['rek_koran'] ?>"target="_blank">
                            <button title="File Rekening" class="btn btn-xs btn-warning"><i class="fa fa-file-pdf-o"></i> Rek </button></a>

                            <a href="<?= MAIN_URL ?>/file/lpj/<?= $a['nilai_lpj'] ?>" target="_blank">
                            <button title="File LPJ" class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"> LPJ </i></button></a>
                        </td>
                             
                    </tr>
                    <?php }
                ?>

                        <tbody>
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->

<!--BELUM MAU DELETE DAN UPDATE-->
<script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var sk = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/delete_lpj.php",
                data: "sk="+sk
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>

    <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>

    <!-- Data Tables -->
    <script>
        $(document).ready(function(){
            $("#tsystem").dataTable({
                "dom":'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'print',
                    //     text: '<i class="fa fa-print"></i> Print'
                    // },
                    { 
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Export to Excel'
                    },
                ],
                /*
                "bProcessing": true,
                "sAjaxSource": "<?=MAIN_URL?>/action/act_data_system_log.php",
                "aoColumns": [
                    {mData: 'tgl_history'},
                    {mData: 'nama_lengkap'},
                    {mData: 'type_history'},
                    {mData: 'tgl_history'},
                    {mData: 'detail_history'}
                ]
                */
            });
        });
    </script>

    
    
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>