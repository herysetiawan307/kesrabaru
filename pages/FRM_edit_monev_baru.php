<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Connection -> Database
    //$db = new Database();
    //$db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="Admin Kesra | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Edit Monitoring";
    $template->startContent();

    // if($_REQUEST["rowsk"]){
    //         $id = $_REQUEST['rowsk'];
    //         $x=mysqli_query($connect,"SELECT * FROM monev WHERE sk='$id'");
    //         while($a=mysqli_fetch_array($x)){
    $id = $_GET['id'];
    $sql = mysqli_query($connect,"SELECT * FROM monev WHERE sk='$id'");
    $result = mysqli_fetch_array($sql);
?>



<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Booking -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/update_monev.php">
                <div style="margin-left:15px">
                    <h4><u>Informasi Umum</u></h4>
                </div>
                <!-- Nama Lengkap -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Select SK</label>
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="sk" placeholder="Select SK" value="<?= $result["sk"]; ?>" readonly>
                    </div>
                </div>
                
                <div class="form-group">
                        <label class="col-sm-2 control-label">Desa</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="desa" placeholder="Nama Desa" value="<?= $result["desa"]; ?>">
                        </div>
                    </div>
                    
                    <!-- lembaga -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Badan/Lembaga Penerima Hibah</label>
                        
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="lembaga" id="lembaga" placeholder="Lembaga" value="<?= $result["lembaga"]; ?>">
                        </div>
                    </div>
                    
                    <!-- Fax Perusahaan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ketua</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="ketua" id="ketua" placeholder="Nama Ketua" value="<?= $result["ketua"]; ?>">
                        </div>
                    </div>
                    
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No.Telp</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="telp" placeholder="Phone" value="<?= $result["telp"]; ?>">
                        </div>
                    </div>
                    
                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Bantuan</label>
                        
                        <div class="col-md-3">
                            <input type="number" class="form-control" name="jumlah_bantuan" placeholder="Jumlah Bantuan" value="<?= $result["jumlah_bantuan"]; ?>">
                        </div>
                    </div>

                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Penerimaan</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tgl_penerimaan" placeholder="yyyy-mm-dd" value="<?= $result["tgl_penerimaan"]; ?>">
                        </div>
                    </div>

                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Penarikan</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tgl_penarikan" placeholder="yyyy-mm-dd" value="<?= $result["tgl_penarikan"]; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hasil Monitoring</label>
                        
                        <div class="col-md-3">
                            <textarea style="resize:none;width:248px;height:100px;" class="form-control" name="hasil_monev" placeholder="Keterangan" value="<?= $result["hasil_monitoring"]; ?>"></textarea>
                        </div>
                    </div>
                    
                    <!-- Rekening koran -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Foto Monitoring(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="monev" value="<?= $result["foto"]; ?>">
                    </div>
                </div>
                    
                    
                <div><span>&nbsp</span></div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>        
                </form>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>