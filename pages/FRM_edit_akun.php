<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";
        
    //Call Template
        $template = new Template();
        
        
                   
        $id = $_REQUEST["rowid"];
        $sql = mysqli_query($connect,"SELECT * FROM akun WHERE nip='$id'");
        $result = mysqli_fetch_array($sql);
?>
            

            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/update_akun.php">
                <div style="margin-left:15px">
                    <h4><u>Edit Data Akun</u></h4>
                </div>
                <!-- Nama Lengkap -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="<?= $result["nama_lengkap"]?>">
                    </div>
                </div>
                
                <!-- NIP Pegawai -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">No Nip</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nip" placeholder="Nip Pegawai" value="<?= $result["nip"]; ?>" readonly>
                    </div>
                </div>
                <!-- Alamat -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:248px;height:100px;" name="alamat" placeholder="Alamat"><?= $result["alamat"]; ?></textarea>
                    </div>
                </div>
                <!-- phone -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="phone" placeholder="Phone" value="<?= $result["telp"]; ?>">
                    </div>
                </div>
                
                <!-- Jabatan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="jabatan">
                                <option value="">---</option>
                                <option value="1">Admin Master</option>
                                <option value="2">Admin Kesra</option>
                                <option value="3">BPK</option>
                        </select>
                        </div>
                </div> 
                 <!-- Username -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="username" placeholder="username" value="<?= $result["username"]; ?>">
                    </div>
                </div>
                 <!-- password -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">password</label>
                        
                    <div class="col-sm-3">
                       <input type="password" class="form-control" name="pass" placeholder="Password" value="<?= $result["passw"]; ?>">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="simpan btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="reset btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>

<script>
    $(document).ready(function(){
        $(".select2").select2();
    });
</script>