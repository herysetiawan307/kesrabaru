<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Connection -> Database
    //$db = new Database();
    //$db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="Admin Kesra | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Info";
    $template->startContent();
?>

<!-- Data -->
<!--<?php
    //$db->select("tb_profil","id_profil,nama,alamat,telepon,fax,email,website,npwp,pkp,logo",NULL,"id_profil='1'");
    //$result_in = $db->getResult();
    //foreach($result_in as $show_in){
    //$id_profil = $show_in["id_profil"];
    //$nama = $show_in["nama"];
    //$alamat = $show_in["alamat"];
    //$telepon = $show_in["telepon"];
    //$fax = $show_in["fax"];
    //$email = $show_in["email"];
    //$website = $show_in["website"];
    //$npwp = $show_in["npwp"];
    //$pkp = $show_in["pkp"];
    //$logo = $show_in["logo"];
    //}
?>-->

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Booking -->
                <form class="form-horizontal" method="POST">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Umum</u></h4>
                    </div>

                     <!-- slogan -->
                     <div class="form-group">
                        <label class="col-sm-4 control-label"></label>
                        
                        <div class="col-md-3">
                            <img src="<?= MAIN_URL ?>/images/logo.png" style="">
                        </div>
                    </div>
                    
                    <!-- Nama Instansi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Instansi</label>
                        
                        <div class="col-md-5">
                        <h5 style="align:left;">Bagian Kesra Kesekertariatan Daerah Kabupaten Badung</h5>
                        </div>
                    </div>
                    <!--<h5 style="align:left;"></h5>-->
                    <!-- Alamat -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat Instansi</label>
                        
                        <div class="col-md-5">
                        <h5 style="align:left;">Jalan Raya Sempidi, Mengwi Badung Bali</h5>
                        </div>
                    </div>
                    
                    <!-- Telepon Perusahaan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon Perusahaan</label>
                        
                        <div class="col-md-2">
                        <h5 style="align:left;">08981776173</h5>
                        </div>
                    </div>
                    
                  
                    
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email Perusahaan</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="email_perusahaan" value="<?= $email; ?>">
                        </div>
                    </div>
                    
                   
                    <!-- slogan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Slogan</label>
                        
                        <div class="col-md-5">
                            <img src="<?= MAIN_URL ?>/images/bg-01.jpg" style="">
                        </div>
                    </div>
                    
                  
                    
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
        
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>