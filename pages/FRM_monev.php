<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Connection -> Database
    //$db = new Database();
    //$db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="Admin Kesra | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Monitoring";
    $template->startContent();
?>



<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Booking -->
                <form class="form-horizontal" method="POST"  action="<?= MAIN_URL ?>/action/add_monitoring.php" enctype="multipart/form-data">
                    <div style="margin-left:15px">
                        <h4><u>Input Data</u></h4>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select SK</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="sk" id="sk" required>
                                <option value="">---</option>
                                <?php
                                    $sql = "SELECT * FROM lpj";
                                    $execute = mysqli_query($connect, $sql);
	                                $result = mysqli_fetch_array($execute);
                                    
                                    foreach($execute as $show_k_a ){
                                ?>
                                    <option value="<?= $show_k_a["sk"]; ?>"><?= $show_k_a['sk'] ?></option>
                                <?php } ?>
                                <option value="Admin Master"></option>
                            </select>
                        </div>
                    </div> 
                    
                    <!-- Alamat -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Desa</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="desa" value="" placeholder="Nama Desa" required>
                        </div>
                    </div>
                    
                    <!-- lembaga -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Badan/Lembaga Penerima Hibah</label>
                        
                        <div class="col-md-5">
                            <textarea style="resize:none;width:337px;height:100px;" type="text" class="form-control" name="lembaga" id="lembaga" placeholder="Lembaga" required readonly></textarea>
                        </div>
                    </div>
                    
                    <!-- Fax Perusahaan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ketua</label>
                        
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="ketua" id="ketua" placeholder="Nama Ketua" required readonly>
                        </div>
                    </div>
                    
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No.Telp</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="telp" placeholder="Phone">
                        </div>
                    </div>
                    
                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Bantuan</label>
                        
                        <div class="col-md-4">
                        <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="jumlah_bantuan" id="nominal" placeholder="Jumlah Bantuan" required readonly>
                        <span class="input-group-addon">.00</span>
                        </div>
                        </div>
                    </div>

                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Penerimaan</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tgl_penerimaan" placeholder="yyyy-mm-dd" required>
                        </div>
                    </div>

                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Penarikan</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tgl_penarikan" placeholder="yyyy-mm-dd" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hasil Monitoring</label>
                        
                        <div class="col-md-3">
                            <textarea style="resize:none;width:337px;height:100px;" class="form-control" name="hasil_monev" value="" placeholder="Keterangan" required></textarea>
                        </div>
                    </div>
                    
                    <!-- Rekening koran -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Foto Monitoring(*PDF)</label>
                        
                    <div class="col-sm-3">
                       <input type="file" class="form-control-file" name="monev" placeholder="Foto Monitoring">
                    </div>
                </div>
                    
                    
                <div><span>&nbsp</span></div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                    
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
        
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

    <!-- select Lembaga -->
    <script>
        $(document).ready(function(){
            $("#sk").change(function(){
                var sk = $("#sk").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/selectlembaga.php",
                    data: "lembaga="+sk,
                    cache: false,  
                })
                .done(function(hasilajax){
                    $('#lembaga').val(hasilajax);
                });

                
            });
        });
    </script>

    <!-- select Ketua -->
    <script>
        $(document).ready(function(){
            $("#sk").change(function(){
                var sk = $("#sk").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/selectKetua.php",
                    data: "sk="+sk,
                    cache: false,  
                })
                .done(function(hasilajax){
                    $('#ketua').val(hasilajax);
                });

                
            });
        });
    </script>

     <!-- select nominal -->
     <script>
        $(document).ready(function(){
            $("#sk").change(function(){
                var sk = $("#sk").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/selectnominal.php",
                    data: "sk="+sk,
                    cache: false,  
                })
                .done(function(hasilajax){
                    $('#nominal').val(hasilajax);
                });

                
            });
        });
    </script>

<!-- End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>