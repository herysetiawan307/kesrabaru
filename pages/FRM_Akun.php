<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";

        
        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="Tambah Akun | Admin Master";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Tambah Akun";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Satuan -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/add_akun.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- Nama Lengkap -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                    </div>
                </div>
                
                <!-- NIP Pegawai -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">No Nip</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nip" placeholder="Nip Pegawai" required>
                    </div>
                </div>
                <!-- Alamat -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                        
                    <div class="col-sm-7">
                        <textarea style="resize:none;width:248px;height:100px;" name="alamat" placeholder="Alamat"></textarea>
                    </div>
                </div>
                <!-- phone -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="phone" placeholder="Phone" required>
                    </div>
                </div>
                
                <!-- Jabatan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="jabatan" required>
                                <option value=""> ---</option>
                                <option value="1">Admin Master</option>
                                <option value="2">Admin Kesra</option>
                                <option value="3">BPK</option>
                        </select>
                        </div>
                </div> 
                 <!-- Username -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="username" placeholder="username" required>
                    </div>
                </div>
                 <!-- password -->
                 <div class="form-group">
                    <label class="col-sm-2 control-label">password</label>
                        
                    <div class="col-sm-3">
                       <input type="password" class="form-control" name="pass" placeholder="Password" required>
                    </div>
                </div>     
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>