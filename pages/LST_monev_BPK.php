<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
 <?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Connection -> Database
    //$db = new Database();
    //$db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="Admin Kesra | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Data Monitoring";
    $template->startContent();
?>

<!-- Log -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover" id="tsystem">
                        <thead>
                            <tr>
                                <td>No.SK</td>
                                <td>Desa</td>
                                <td>Badan/Lembaga</td>
                                <td>Ketua</td>
                                <td>No.Telp</td>
                                <td>Jumlah Bantuan</td>
                                <td>Tgl.Penarimaan</td>
                                <td>Tgl.Penarikan</td>
                                <td>Hasil Monev</td>
                                <td>Foto</td>
                               
                            </tr>
                        </thead>
                        <?php
				//data 	BELOM MAU		
                $x=mysqli_query($connect,"SELECT * FROM monev");
                
                while($a=mysqli_fetch_array($x)){?>
                    <tr>   
                    <td><?= $a['sk'] ?></td>
                        <td><?= $a['desa'] ?></td>
                        <td><?= $a['lembaga'] ?></td>
                        <td><?= $a['ketua'] ?></td>
                        <td><?= $a['telp'] ?></td>
                        <td><?= $a['jumlah_bantuan'] ?></td>
                        <td><?= $a['tgl_penerimaan'] ?></td>
                        <td><?= $a['tgl_penarikan'] ?></td>
                        <td><?= $a['hasil_monitoring'] ?></td>
                        <td>
                             <a href="<?= MAIN_URL ?>/file/monev/<?= $a['foto'] ?>"target="_blank">
                             <button class="btn btn-xs bg-olive"><i class="fa fa-file-pdf-o"></i> Monitoring </button></a>
                        </td>
                              
                    </tr>
                    <?php }
                ?>


                        <tbody>
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php
    $template->endContent();
?>

<?php
    $template->startModal();
    $template->conModal();
    $template->footModal();
?>
<!-- Place Script Here -->

<!--BELUM MAU DELETE DAN UPDATE-->
<script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var sk = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/delete_monev.php",
                data: "sk="+sk
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>

    <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>

    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('#myModal').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('sk');
                
                $.ajax({
                    type : 'post',
                    url : 'FRM_edit_monev.php',
                    data :  'rowsk='+ rowid,
                    success : function(data){
                    $('.fetched-data').html(data);
                    }
                });
            });
        });
    </script> -->

    <!-- Data Tables -->
    <script>
        $(document).ready(function(){
            $("#tsystem").dataTable({
                "dom":'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'print',
                    //     text: '<i class="fa fa-print"></i> Print'
                    // },
                    { 
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Export to Excel'
                    },
                ],
                /*
                "bProcessing": true,
                "sAjaxSource": "<?=MAIN_URL?>/action/act_data_system_log.php",
                "aoColumns": [
                    {mData: 'tgl_history'},
                    {mData: 'nama_lengkap'},
                    {mData: 'type_history'},
                    {mData: 'tgl_history'},
                    {mData: 'detail_history'}
                ]
                */
            });
        });
    </script>

    
    
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>