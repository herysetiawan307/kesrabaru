-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2019 at 06:20 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kesradm`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `nama_lengkap` varchar(225) NOT NULL,
  `nip` char(15) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `telp` char(13) NOT NULL,
  `lvl` char(3) NOT NULL,
  `username` varchar(225) NOT NULL,
  `passw` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`nama_lengkap`, `nip`, `alamat`, `telp`, `lvl`, `username`, `passw`) VALUES
('putu heri setiawan', '993887646464', 'sempidi badung', '08981776173', '1', 'putuheri', 'd8f1f2026c9ed9750139e154d32cceaea833d1d3'),
('a yudi', '21342432', 'asdsfsdf', '235435435', 'BPK', 'yudi', '7453b923f6da0a823df0f50ebe786dcad9942576');

-- --------------------------------------------------------

--
-- Table structure for table `lpj`
--

CREATE TABLE `lpj` (
  `sk` char(4) NOT NULL,
  `lembaga` varchar(225) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `peruntukan` varchar(225) NOT NULL,
  `tgl_pengumpulan` date NOT NULL,
  `ketua` varchar(225) NOT NULL,
  `alamat_ketua` varchar(225) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `jml_hibah` int(225) NOT NULL,
  `rek_koran` varchar(225) NOT NULL,
  `nilai_lpj` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpj`
--

INSERT INTO `lpj` (`sk`, `lembaga`, `alamat`, `peruntukan`, `tgl_pengumpulan`, `ketua`, `alamat_ketua`, `phone`, `jml_hibah`, `rek_koran`, `nilai_lpj`) VALUES
('002', 'test', '', 'pembangunan', '2019-05-30', 'hery setiawan', 'sempidi', '08981776173', 1000000, '', ''),
('12', '12', '12', '12', '2019-05-15', '12', '12', '12', 12, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('13', '13', '13', '13', '2019-05-02', '13', '13', '13', 13, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('14', '14', '14', '14', '0000-00-00', '14', '14', '14', 12, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('2', 'b', 'n', 'n', '0000-00-00', 'n', 'n', '', 0, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('213', '3', '3', '3', '0000-00-00', '3', '4', '6', 6, 'Laporan KP.pdf', ''),
('22', 'd', 'dnm,', 'dmw', '0000-00-00', '', '', '', 0, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('6', '6', '6', '6', '0000-00-00', '6', '6', '6', 6, 'Laporan KP.pdf', ''),
('7', '7', '7', '7', '0000-00-00', '7', '7', '7', 7, 'Laporan KP.pdf', ''),
('8', '8', '8', '8', '0000-00-00', '8', '8', '8', 8, 'Laporan KP.pdf', ''),
('9', '9', '9', '9', '2019-05-01', '9', '9', '9', 9, 'Laporan KP.pdf', ''),
('90', '', '', '', '0000-00-00', '', '', '', 0, 'Laporan KP.pdf', 'Laporan KP.pdf'),
('99', '', '', '', '0000-00-00', '', '', '', 0, 'Laporan KP.pdf', 'Laporan KP.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `monev`
--

CREATE TABLE `monev` (
  `sk` varchar(4) NOT NULL,
  `desa` varchar(225) NOT NULL,
  `lembaga` varchar(225) NOT NULL,
  `ketua` varchar(225) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `jumlah_bantuan` int(225) NOT NULL,
  `tgl_penerimaan` date NOT NULL,
  `tgl_penarikan` date NOT NULL,
  `hasil_monitoring` varchar(225) NOT NULL,
  `foto` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monev`
--

INSERT INTO `monev` (`sk`, `desa`, `lembaga`, `ketua`, `telp`, `jumlah_bantuan`, `tgl_penerimaan`, `tgl_penarikan`, `hasil_monitoring`, `foto`) VALUES
('2132', 'nbmnbmn', 'gjhvhhj', 'jhbhjbhj', '89897897', 56656576, '0000-00-00', '2019-05-01', 'gjhgshjbjdfsf', ''),
('656', 'kkkmm', 'uii', 'jjkjk', '090988', 0, '0000-00-00', '2019-05-02', 'kkkj', ''),
('6567', 'hvvnnb', 'hvjhvhj', 'hhjvhjbjh', '76578687', 44576789, '2019-05-06', '2019-05-31', 'gfgxfgchjby', ''),
('3243', 'nkjnjk', 'jkbkjnjknkjn', 'jbkjjknjk', '768687687', 698798789, '2019-05-02', '2019-05-31', 'asdsfsdf', 'Laporan KP.pdf'),
('2324', 'sdfdsfds', 'sdfdsfsdf', 'jhjkbjkb', '768769879', 8978, '2019-05-02', '2019-05-30', 'dsnfkndskjfsdf', 'Laporan KP.pdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `lpj`
--
ALTER TABLE `lpj`
  ADD PRIMARY KEY (`sk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
