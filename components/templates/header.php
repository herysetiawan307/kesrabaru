<body class="hold-transition skin-red sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?= MAIN_URL ?>/beranda.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?=MAIN_URL?>/components/images/icon-bhumiku2.png"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?=MAIN_URL?>/components/images/icon-bhumiku2.png"> <strong>KESRA</strong></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a><i class="fa fa-calendar"></i>  <span id="clock"></span></a>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=MAIN_URL?>/components/images/user1.png" class="user-image" alt="User Image">
                <span class="hidden-xs"></span>
                
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?=MAIN_URL?>/components/images/user1.png" class="img-circle" alt="User Image">

                  <p>
                    <!--nama lengkap-->
                      <p>Admin_<?= $_SESSION['nama_lengkap']?></p>
                    <!--jabatan-->      
                      
                  </p>
                </li>
                <!-- Menu Body -->

                <!-- Menu Footer-->
                <li class="user-footer">
                    <div>
                        <a href="<?= MAIN_URL ?>/logout.php" class="btn btn-danger btn-block logout">Sign out <i class="fa fa-sign-out"></i></a>
                  </div>
                </li>
              </ul>
            </li>
        </ul>
      </div>
    </nav>
  </header>