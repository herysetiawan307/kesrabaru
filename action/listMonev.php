<?php
    //Required File
    require_once dirname(__FILE__)."/../class/native_connect.php";

    //select
    $query = "SELECT * FROM monev";
    $sql = mysqli_query($connect,$query) or die(mysqli_error());
    while($result=mysqli_fetch_array($sql)){
        $data[] = $result;
    }

    $finalResult = [
                "sEcho"=>1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData"=> $data
    ];

    echo json_encode($finalResult)
?>